﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Intro : MonoBehaviour
{
	public string[] intros;
	public Text bubbleText;

	private int currentIndex = 0;

	private void Start()
	{
		currentIndex = 0;
		if (intros.Length > 0)
			bubbleText.text = intros[0];
		else
			gameObject.SetActive(false);
	}

	public void AdvanceIntro()
	{
		currentIndex++;
		if (intros.Length > currentIndex)
			bubbleText.text = intros[currentIndex];
		else
			gameObject.SetActive(false);
	}
}
