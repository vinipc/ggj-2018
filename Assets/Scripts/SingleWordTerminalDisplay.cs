﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SingleWordTerminalDisplay : TerminalDisplay
{
	public Text wordDisplay;

	public override void Open(WordTerminal terminal)
	{
		base.Open(terminal);
		wordDisplay.text = terminal.outputWord;
	}
}
