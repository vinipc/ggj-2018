﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using DG.Tweening;

public abstract class WordTerminal : MonoBehaviour
{
	private const string QUERY = "images.google.com/images?q={0}&hl=en&imgsz=Large";
	private const string START_STRING = "http://t0.gstatic.com";
	private const string FINAL_STRING = "\"";

	public bool isHidden = false;
	public Texture2D noiseTexture;
	public Sprite pcOnSprite;
	public Sprite pcOffSprite;
	public SpriteRenderer spriteRenderer;
	public SpriteRenderer outputImage;
	public Image loadingImage;
	public string outputWord;

	[HideInInspector]
	public WordTerminal parentTerminal;
	[HideInInspector]
	public Texture2D outputTexture;

	private Vector3 outputImageScale;

	protected virtual void Awake()
	{
		outputImageScale = outputImage.transform.localScale;
	}

	private void OnMouseDown()
	{
		if (!string.IsNullOrEmpty(outputWord))
			OpenDisplay();
	}

	public virtual void UpdateWord()
	{
		spriteRenderer.sprite = string.IsNullOrEmpty(outputWord) ? pcOffSprite : pcOnSprite;
	}

	protected void UpdateOutputImage()
	{
		StopAllCoroutines();

		if (!isHidden)
			StartCoroutine(ImageUpdaterCoroutine());
		else
		{
			Rect rect = new Rect(0f, 0f, noiseTexture.width, noiseTexture.height);
			outputImage.sprite = Sprite.Create(noiseTexture, rect, new Vector2(0.5f, 0.5f));
			AnimateImage();

			if (parentTerminal != null)
				parentTerminal.UpdateWord();
		}
	}

	private IEnumerator ImageUpdaterCoroutine()
	{
		outputImage.sprite = null;

		if (string.IsNullOrEmpty(outputWord))
		{
			if (parentTerminal!= null)
				parentTerminal.UpdateWord();
			yield break;
		}

		Debug.Log("Fetching image for <" + outputWord + "> on " + name);
		string searchUrl = string.Format(QUERY, outputWord);
		WWW www = new WWW(searchUrl);

		while (!www.isDone)
		{
			loadingImage.fillAmount = www.progress / 2f;
			yield return 0;
		}
		loadingImage.fillAmount = 0.5f;
		Debug.Log("Fetched web page for <" + outputWord + "> on " + name);

		int startingIndex = www.text.IndexOf(START_STRING);
		int finalIndex = www.text.IndexOf(FINAL_STRING, startingIndex);
		int urlLength = finalIndex - startingIndex;
		string imageUrl = www.text.Substring(startingIndex, urlLength);
		Debug.Log("Found image url:\n" + imageUrl);

		www = new WWW(imageUrl);

		while (!www.isDone)
		{
			loadingImage.fillAmount = 0.5f + www.progress / 2f;
			yield return 0;
		}
		loadingImage.fillAmount = 0f;
		Debug.Log("Fetched image for <" + outputWord + "> on " + name);

		outputTexture = www.texture;
		Rect rect = new Rect(0f, 0f, outputTexture.width, outputTexture.height);
		outputImage.sprite = Sprite.Create(outputTexture, rect, new Vector2( 0.5f, 0.5f));
		AnimateImage();

		if (parentTerminal != null)
			parentTerminal.UpdateWord();
	}

	public void OpenDisplay()
	{		
		GameUI ui = FindObjectOfType<GameUI>();
		if (this is MultiWordsTerminal)
			ui.OpenMultiWordDisplay(this);
		else
			ui.OpenSingleWordDisplay(this);

		FindObjectOfType<TracksMaster>().HighlightTracks(this);
	}

	public void AnimateImage()
	{
		if (parentTerminal == null)
			return;

		DOTween.Kill(this);

		outputImage.transform.localPosition = Vector3.zero;
		outputImage.transform.localScale = outputImageScale;

		Sequence sequence = DOTween.Sequence();
		sequence.AppendInterval(0.1f);
		sequence.Append(outputImage.transform.DOMove(parentTerminal.transform.position, 5f).SetEase(Ease.Linear));
		sequence.Join(outputImage.transform.DOScale(Vector3.zero, 2.5f).From().SetLoops(2, LoopType.Yoyo));
		sequence.SetLoops(-1, LoopType.Restart);
		sequence.SetId(this);
	}
}
