﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TracksMaster : Singleton<TracksMaster>
{
	public List<AudioSource> availableTracks = new List<AudioSource>();

	private AudioSource audioSource;
	private Dictionary<WordTerminal, AudioSource> terminalToAudio = new Dictionary<WordTerminal, AudioSource>();
	private List<AudioSource> activeSources = new List<AudioSource>();

	private void Awake()
	{
		audioSource = GetComponent<AudioSource>();
		SingleWordTerminal[] terminals = FindObjectsOfType<SingleWordTerminal>();
		for (int i = 0; i < terminals.Length; i++)
		{
			Debug.Log("Adding " + terminals[i].name + " to dictionary");
			AudioSource randomSource = availableTracks.GetRandom();
			availableTracks.Remove(randomSource);
			terminalToAudio.Add(terminals[i], randomSource);
		}
	}

	private void Start()
	{
		audioSource.volume = 0f;
		audioSource.Play();
		audioSource.DOFade(1f, 0.5f);

		foreach(KeyValuePair<WordTerminal, AudioSource> entry in terminalToAudio)
		{
			entry.Value.volume = 0f;
			entry.Value.Play();
		}
	}

	public void StartTrack(WordTerminal terminal)
	{
		Debug.Log("Starting track for " + terminal.name);
		AudioSource source = terminalToAudio[terminal];
		source.DOFade(1f, 0.5f);

		if (!activeSources.Contains(source))
			activeSources.Add(source);
	}

	public void StopTrack(WordTerminal terminal)
	{
		Debug.Log("Stopping track for " + terminal.name);
		AudioSource source = terminalToAudio[terminal];
		source.DOFade(0f, 0.5f);

		activeSources.Remove(source);
	}

	public void HighlightTracks(WordTerminal terminal)
	{
		List<WordTerminal> sourceTerminals = new List<WordTerminal>();
		if (terminal is MultiWordsTerminal)
			sourceTerminals = ((MultiWordsTerminal) terminal).GetSingleTerminals();
		else
			sourceTerminals.Add(terminal);
		
		foreach(KeyValuePair<WordTerminal, AudioSource> entry in terminalToAudio)
		{
			if (!sourceTerminals.Contains(entry.Key) && activeSources.Contains(entry.Value))
			{
				entry.Value.DOFade(0.25f, 0.5f);
			}
		}
	}

	public void DeHightlightTracks()
	{
		foreach(KeyValuePair<WordTerminal, AudioSource> entry in terminalToAudio)
		{
			if (activeSources.Contains(entry.Value))
			{
				entry.Value.DOFade(1f, 0.5f);
			}
		}
	}
}