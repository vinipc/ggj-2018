﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultiWordsTerminal : WordTerminal
{
	private const float DOT_STEP = 0.5f;

	public Transform dotPrefab;
	public WordTerminal[] sourceTerminals;
	public Text debugText;

	protected override void Awake()
	{
		base.Awake();
		for (int i = 0; i < sourceTerminals.Length; i++)
		{			
			sourceTerminals[i].parentTerminal = this;
			Vector3 targetPosition = sourceTerminals[i].transform.position;
			float distance = (targetPosition - transform.position).sqrMagnitude;
			Vector3 step = (targetPosition - transform.position).normalized * DOT_STEP;
			int j = 0;
			while ((transform.position + j * step - transform.position).sqrMagnitude < distance)
			{
				Instantiate<Transform>(dotPrefab, transform.position + j * step, Quaternion.identity, transform);
				j++;
			}
		}
	}

	public override void UpdateWord()
	{
		string combinedWords = string.Empty;

		for (int i = 0; i < sourceTerminals.Length; i++)
		{
			if (string.IsNullOrEmpty(sourceTerminals[i].outputWord) || combinedWords.Contains(sourceTerminals[i].outputWord))
			{
				combinedWords = string.Empty;
				break;
			}

			combinedWords = string.Concat(combinedWords, " ", sourceTerminals[i].outputWord);
		}

		outputWord = combinedWords.Trim();
		UpdateOutputImage();
		base.UpdateWord();
	}

	public List<WordTerminal> GetSingleTerminals()
	{
		List<WordTerminal> terminals = new List<WordTerminal>();
		for (int i = 0; i < sourceTerminals.Length; i++)
		{
			if (sourceTerminals[i] is SingleWordTerminal || sourceTerminals[i] is MysteryWordTerminal)
				terminals.Add(sourceTerminals[i]);
			else
			{
				MultiWordsTerminal multiWordsTerminal = (MultiWordsTerminal) sourceTerminals[i];
				terminals.AddRange(multiWordsTerminal.GetSingleTerminals());
			}
		}

		return terminals;
	}
}
