﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameUI : MonoBehaviour
{
	public string nextLevel;
	public SingleWordTerminalDisplay singleWordDisplay;
	public MultiWordTerminalDisplay multiWordsDisplay;
	public Button nextLevelButton;

	public AudioClip openDisplayClip;
	private AudioSource audioSource;

	public void Awake()
	{
		nextLevelButton.onClick.AddListener(NextLevelButtonPressed);
		nextLevelButton.gameObject.SetActive(false);
		audioSource = GetComponent<AudioSource>();
	}

	public void OpenSingleWordDisplay(WordTerminal terminal)
	{
		audioSource.PlayOneShot(openDisplayClip);
		singleWordDisplay.Open(terminal);
	}

	public void OpenMultiWordDisplay(WordTerminal terminal)
	{
		audioSource.PlayOneShot(openDisplayClip);
		multiWordsDisplay.Open(terminal);
	}

	public void ActivateNextLevelButton()
	{
		nextLevelButton.gameObject.SetActive(true);
	}

	public void NextLevelButtonPressed()
	{
		SceneManager.LoadScene(nextLevel);
	}
}
