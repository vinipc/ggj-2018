﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;



public class MysteryRandomizer : MonoBehaviour {

    List<string> mysteryList = new List<string>();
    
    // Use this for initialization
    void Awake()
    {
        TextAsset mysteryTxtReader = Resources.Load<TextAsset>("nounlist");
        //print(asd.text);
        Char sep = '\n';
        string[] subStrings = mysteryTxtReader.text.Split(sep);

        for (int i = 0; i < subStrings.Length; i++)
        {
            if (!subStrings[i].StartsWith(" ") && !string.IsNullOrEmpty(subStrings[i].Trim()))
            {
                mysteryList.Add(subStrings[i]);
            }
        }
        string misteryWord = mysteryList[UnityEngine.Random.Range(0, mysteryList.Count)];
		GetComponent<MysteryWordTerminal>().outputWord = misteryWord.Trim();
    }
}
