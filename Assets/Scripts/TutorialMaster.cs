﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialMaster : MonoBehaviour
{
	private SingleWordTerminal[] singleWordTerminals;

	private void Awake()
	{
		singleWordTerminals = FindObjectsOfType<SingleWordTerminal>();
	}

	private void Update()
	{
		for (int i = 0; i < singleWordTerminals.Length; i++)
		{
			if (string.IsNullOrEmpty(singleWordTerminals[i].outputWord))
				return;
		}

		FindObjectOfType<GameUI>().ActivateNextLevelButton();
		this.enabled = false;
	}
}
