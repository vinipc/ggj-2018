﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SingleWordTerminal : WordTerminal
{
	public InputField wordInput;
	public AudioClip[] keyClips;
	private AudioSource audioSource;

	protected override void Awake()
	{
		base.Awake();
		audioSource = GetComponent<AudioSource>();
	}

	public override void UpdateWord()
	{
		outputWord = wordInput.text.Trim();		
		UpdateOutputImage();
		base.UpdateWord();

		if (string.IsNullOrEmpty(outputWord) || isHidden)
		{
			TracksMaster.instance.StopTrack(this);
		}
		else
		{
			TracksMaster.instance.StartTrack(this);
		}
	}

	public void InputFieldUpdated()
	{
		wordInput.text = wordInput.text.Trim();
		AudioClip clip = keyClips[Random.Range(0, keyClips.Length)];
		audioSource.clip = clip;
		audioSource.volume = Random.Range(0.9f, 1.1f);
		audioSource.Play();
	}
}
