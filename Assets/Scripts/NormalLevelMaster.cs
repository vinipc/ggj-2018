﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalLevelMaster : MonoBehaviour
{
	private MysteryWordTerminal[] mysteryTerminals;

	private void Awake()
	{
		mysteryTerminals = FindObjectsOfType<MysteryWordTerminal>();
	}

	private void Update()
	{
		for (int i = 0; i < mysteryTerminals.Length; i++)
		{
			if (!mysteryTerminals[i].guessed)
				return;
		}

		FindObjectOfType<GameUI>().ActivateNextLevelButton();
		this.enabled = false;
	}
}
