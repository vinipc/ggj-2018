﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MysteryWordTerminal : WordTerminal
{
	public InputField wordInput;
	public bool guessed = false;
	public AudioClip[] keyClips;
	public AudioClip winClip;
	private AudioSource audioSource;

	protected override void Awake ()
	{
		base.Awake();
		audioSource = GetComponent<AudioSource>();
	}

	private void Start()
	{
		guessed = false;
		UpdateOutputImage();		
	}

	public void CheckInputWord()
	{
		Debug.Log("Comparing " + wordInput.text + " to " + outputWord);
		if (wordInput.text == outputWord)
			CorrectAnswer();
		else
			WrongAnswer();
	}

	private void CorrectAnswer()
	{
		guessed = true;
		wordInput.interactable = false;
		ColorBlock newColors = wordInput.colors;
		newColors.disabledColor = Color.green;
		wordInput.colors = newColors;
		isHidden = false;

		wordInput.transform.DOMove(Vector3.up * 0.3f, 0.1f).SetRelative().SetLoops(2, LoopType.Yoyo).SetEase(Ease.OutQuad);

		UpdateOutputImage();
		GetComponent<BoxCollider2D>().enabled = true;
		audioSource.PlayOneShot(winClip);
	}

	private void WrongAnswer()
	{
		wordInput.text = string.Empty;
		ColorBlock originalColors = wordInput.colors;

		Sequence sequence = DOTween.Sequence();
		sequence.AppendCallback(()=> wordInput.SetColor(Color.red));
		sequence.AppendInterval(0.1f);
		sequence.AppendCallback(()=> wordInput.SetColor(originalColors.normalColor));
		sequence.AppendInterval(0.1f);
		sequence.SetLoops(2, LoopType.Restart);
		sequence.OnComplete(()=> wordInput.colors = originalColors);
	}

	public void InputFieldUpdated()
	{
		wordInput.text = wordInput.text.Trim();
		AudioClip clip = keyClips[Random.Range(0, keyClips.Length)];
		audioSource.clip = clip;
		audioSource.volume = Random.Range(0.9f, 1.1f);
		audioSource.Play();
	}
}