﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TerminalDisplay : MonoBehaviour
{
	public Image outputImage;

	protected Vector2 outputMaxSize;

	protected virtual void Awake()
	{
		outputMaxSize = outputImage.rectTransform.rect.size;
	}

	public virtual void Open(WordTerminal terminal)
	{
		gameObject.SetActive(true);
		outputImage.SetTexture(terminal.outputTexture, outputMaxSize);
	}

	public virtual void Close()
	{
		gameObject.SetActive(false);
		FindObjectOfType<TracksMaster>().DeHightlightTracks();
	}
}
