﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class ExtensionMethods
{
	public static void SetTexture(this Image image, Texture2D texture)
	{
		Rect rect = new Rect(0f, 0f, texture.width, texture.height);
		image.sprite = Sprite.Create(texture, rect, new Vector2(0.5f, 0.5f));
		image.rectTransform.sizeDelta = rect.size;
	}

	public static void SetTexture(this Image image, Texture2D texture, Vector2 maxSize)
	{
		image.SetTexture(texture);
		float ratio = 1f;

		if (image.rectTransform.rect.width > image.rectTransform.rect.height)
			ratio = maxSize.x / image.rectTransform.rect.width;
		else
			ratio = maxSize.y / image.rectTransform.rect.height;

		image.rectTransform.sizeDelta *= ratio;
	}

	public static void SetColor(this InputField inputField, Color color)
	{
		ColorBlock colors = inputField.colors;
		colors.disabledColor = color;
		colors.normalColor = color;
		colors.highlightedColor = color;
		colors.pressedColor = color;
		inputField.colors = colors;
	}

	public static T GetRandom<T>(this List<T> list)
	{
		return list[Random.Range(0, list.Count)];
	}
}
