﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultiWordTerminalDisplay : TerminalDisplay
{
	public Image[] inputImageDisplays;
	public Texture2D noise;

	private Vector2 inputMaxSize;

	protected override void Awake()
	{
		base.Awake();
		inputMaxSize = inputImageDisplays[0].rectTransform.rect.size;
	}

	public override void Open(WordTerminal terminal)
	{
		base.Open(terminal);

		for (int i = 0; i < inputImageDisplays.Length; i++)
		{
			Texture2D texture = ((MultiWordsTerminal) terminal).sourceTerminals[i].outputTexture;
			if (texture == null)
				texture = noise;
			inputImageDisplays[i].SetTexture(texture, inputMaxSize);
		}
	}
}
